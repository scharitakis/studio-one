function createTab4(win4) {
	self = win4
	
	
	/*----------------------info button--------*/
	var infolight = Titanium.UI.createButton({systemButton: Titanium.UI.iPhone.SystemButton.INFO_LIGHT});
	self.rightNavButton = infolight;
	
	infolight.addEventListener('click', function()
	{
		var info_webview = Ti.UI.createWebView({
			color:'#D2DFC3',
			backgroundColor:"transparent",
			font:{fontSize:12,fontWeight:'bold', fontFamily:'Arial'},
			top:12,
			height:"95%",
			width:"90%"
			
		});
		
		
		var myscript = '<script>'+
					'function test(){'+
					'var email = document.getElementById("email"); '+
					'var site1 = document.getElementById("site1"); '+
					'var site2 = document.getElementById("site2"); '+
					'var tel = document.getElementById("tel"); '+
					'if(email){'+
					'email.onclick=function(){'+
					'Ti.App.fireEvent("openEmail",{ email:"info@social-net.gr"});'+
					'}}'+
					'if(site1){'+
					'site1.onclick=function(){'+
					'Ti.App.fireEvent("openURL",{ url:"http://www.studioone.gr"});'+
					'}}'+
					'if(site2){'+
					'site2.onclick=function(){'+
					'Ti.App.fireEvent("openURL",{ url:"http://www.social-net.gr"});'+
					'}}'+
					'if(tel){'+
					'tel.onclick=function(){'+
					'Ti.App.fireEvent("opentel",{ tel:"00306948527462"});'+
					'}}'+
					'} </script>';
					
		var info_html='<!DOCTYPE html>'+
		'<html lang="en"> <head>'+myscript+					
		'<meta http-equiv="Content-Type" content="text/html; charset=utf-8">'+
		'<meta name="format-detection" content="telephone=no">'+
		'<style>body { -webkit-text-size-adjust:none;} </style>'+
		'</head>'+
		'<body onload="test();" style="font-family:Arial;font-weight:bold;font-size:14px;margin:0;padding:0;color:#D2DFC3;">'+
		'<div style="-webkit-border-radius:15px;padding:15px;">'+
		'<div style="width:100%">'+
		'<div style="width:100%;text-align:center;font-weight:bold;font-size:18px;">Σχετικά με την εφαρμογή...</div>'+
		'<div style="margin-top:10px;">Studio One: Η συγκεκριμένη εφαρμογή σχεδιάστηκε και υλοποιήθηκε '+
		'από την Social Network Support.</div>'+
		'<div style="margin-top:10px;">Όλα τα δεδομένα προέρχονται από την Studio One <u><span id="site1"> www.studioone.gr</span></u></div>'+
		'<div style="margin-top:10px;">Επικοινωνία:</div>'+
		'<div>SNS | Social Network Support</div>'+
		'<div style="margin-top:10px;"> - Social Media Marketing and Strategy Agency</div>'+
		'<div> - Mobile Applications</div>'+
		'<div id="email" style="margin-top:10px;">E-mail: <u>info@social-net.gr</u></div>'+
		'<div id="site2">Web: <u> www.social-net.gr</u></div>'+
		'<div id="tel" style=>Mob: <u>+30 6948527462</u></div>'+
		'</div></div>'+
		'</body></html>'
		
		info_webview.setHtml(info_html);
		info_win.add(info_webview);
		info_win.open({modal:true});
	});
	
	var info_win = Titanium.UI.createWindow({
			backgroundImage:"images/bg_app.jpg",
			title:'Social Network Support',
			barColor:'black'
		});
	var info_win_btn_close = Titanium.UI.createButton({
			title:'Πίσω',
			style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
		});
		
	info_win.setLeftNavButton(info_win_btn_close);
	info_win_btn_close.addEventListener('click',function()
		{
			info_win.remove(info_win.children[1])
			info_win.close();
		});
	
	var info_winview = Titanium.UI.createView({									
									width:'95%',
									height:'95%',
									borderRadius:10,
									backgroundColor:"black",
									opacity:"0.6",
									top:10,
									
								});
	info_win.add(info_winview);
	
	

	
	
	
	function sendEmail(){
		var emailDialog = Ti.UI.createEmailDialog()
    	emailDialog.subject = "StudioOne";
    	emailDialog.toRecipients = ['Info@studioone.gr'];
    	emailDialog.messageBody = '';
    	emailDialog.open();
		
	};
	
	
	//win4.addEventListener('open',function(){
		
		var studione_place = Titanium.Map.createAnnotation({
			latitude:37.941573,
			longitude:23.740101,
			title:"StudiOne",
			subtitle:'Αθήνα:Λ. Βουλιαγμένης 270, Αγ. Δημήτριος 17342',
			pincolor:Titanium.Map.ANNOTATION_BLUE,
			animate:true,
			leftButton: 'images/logo2_30.png',
			myid:1 // CUSTOM ATTRIBUTE THAT IS PASSED INTO EVENT OBJECTS
		});

		var mapview = Titanium.Map.createView({
			mapType: Titanium.Map.STANDARD_TYPE,
			region: {latitude:37.941573, longitude:23.740101, latitudeDelta:0.01, longitudeDelta:0.01},
			animate:true,
			regionFit:true,
			top:5,
			height:180,
			width:'95%',
			annotations:[studione_place]
		});
		mapview.selectAnnotation(studione_place);
		self.add(mapview);
		
		
		mapview.addEventListener('click',function(evt)
		{
			var annotation = evt.annotation;
			var title = evt.title;
			var clickSource = evt.clicksource;
			if(clickSource =="leftButton")
			{
				sendEmail();
			}
			
		});
		
		var contact_lbl = Ti.UI.createLabel({
				text:"Contact",
				color:"#dddddd",
				font:{fontSize:18,fontWeight:'bold'},
				width:"70",
				height:'auto',
				top:195,
				left:10,
		});
		
		var sharing_tube_btn = Ti.UI.createButton({
				backgroundImage :'images/youtube.png',
				width:35,
				height:35,
				top:187,
				right:10,
		});
		
		sharing_tube_btn.addEventListener("click",function(){
			Ti.Platform.openURL("http://m.youtube.com/#/user/studioonevideos");
		})
		
		var sharing_tw_btn = Ti.UI.createButton({
				backgroundImage :'images/twitter.png',
				width:35,
				height:35,
				top:187,
				right:50,
		});
		
		sharing_tw_btn.addEventListener("click",function(){
			Ti.Platform.openURL("https://mobile.twitter.com/StudiOne_Xiros");
			
			
		})
		
		var sharing_fb_btn = Ti.UI.createButton({
				backgroundImage :'images/facebook.png',
				width:35,
				height:35,
				top:187,
				right:90,
		});
		
		sharing_fb_btn.addEventListener("click",function(){
			Ti.Platform.openURL("https://m.facebook.com/StudiOneXiros");
			//Ti.Platform.openURL("http://www.facebook.com/StudiOneXiros");
			
		})
		
		var arrow_img = Ti.UI.createImageView({
			image:'images/listArrow-gray.png',
			top:12,
			right:5,
			width:25,
			height:20
		});
		
		var arrow1_img = Ti.UI.createImageView({
			image:'images/listArrow-gray.png',
			top:12,
			right:5,
			width:25,
			height:20
		});
		
		var arrow2_img = Ti.UI.createImageView({
			image:'images/listArrow-gray.png',
			top:12,
			right:5,
			width:25,
			height:20
		});
		
		var place_view = Ti.UI.createView({
			top:225,
			backgroundColor:"#cccccc",
			width: '95%',
			height: '40',
			borderRadius:6
		});
		
		var place1_lbl = Ti.UI.createLabel({
				text:"Λ. Βουλιαγμένης 270,",
				color:"black",
				font:{fontSize:12,fontWeight:"bold"},
				width:"auto",
				height:'auto',
				top:3,
				left:50,
		});
		
		var place2_lbl = Ti.UI.createLabel({
				text:"Αγ. Δημήτριος, τ.κ: 17342",
				color:"black",
				font:{fontSize:12,fontWeight:"bold"},
				width:"auto",
				height:'auto',
				top:20,
				left:50,
		});
		
		var map_img = Ti.UI.createImageView({
			image:'images/103-map.png',
			top:10,
			left:10,
			width:26,
			height:21
		});
		place_view.add(map_img);
		place_view.add(arrow_img);
		place_view.add(place1_lbl);
		place_view.add(place2_lbl);
		
		place_view.addEventListener("touchstart",function(){
			place_view.backgroundColor="#d2dfc3";
			
		});
		place_view.addEventListener("touchend",function(){
			place_view.backgroundColor="#cccccc";
			
		});
		place_view.addEventListener("touchcancel",function(){
			place_view.backgroundColor="#cccccc";
			
		});
		
		place_view.addEventListener('click',function()
		{
			if(Titanium.Geolocation.locationServicesEnabled === false)
			{
				Titanium.UI.createAlertDialog({title:'Studioone', message:'Your device has geo turned off - turn it on.'}).show();
			}else
			{
				
				Titanium.Geolocation.getCurrentPosition(function(e)
				{
					if (!e.success || e.error)
					{	
						return;
					}
			
					var longitude = e.coords.longitude;
					var latitude = e.coords.latitude;
					var altitude = e.coords.altitude;
					var heading = e.coords.heading;
					var accuracy = e.coords.accuracy;
					var speed = e.coords.speed;
					var timestamp = e.coords.timestamp;
					var altitudeAccuracy = e.coords.altitudeAccuracy;
					
					Ti.Platform.openURL("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr=37.941573,23.740101");
				});
			}
			
		});
		
		var tel_view = Ti.UI.createView({
			top:270,
			backgroundColor:"#cccccc",
			width: '95%',
			height: '40',
			borderRadius:6
		});
		
		var tel_img = Ti.UI.createImageView({
			image:'images/75-phone.png',
			top:10,
			left:10,
			width:26,
			height:21
		});
		
		var tel1_lbl = Ti.UI.createLabel({
				text:"Κιν: 694 464 0082",
				color:"black",
				font:{fontSize:12,fontWeight:"bold"},
				width:"auto",
				height:'auto',
				top:2,
				left:50,
		});
		var tel2_lbl = Ti.UI.createLabel({
				text:"Τηλ: 210 971 9486, 210 866 4108",
				color:"black",
				font:{fontSize:12,fontWeight:"bold"},
				width:"auto",
				height:'auto',
				top:20,
				left:50,
		});
		tel_view.add(tel_img);
		tel_view.add(tel1_lbl);
		tel_view.add(tel2_lbl);
		tel_view.add(arrow1_img);
		
		tel_view.addEventListener("touchstart",function(){
			tel_view.backgroundColor="#d2dfc3";
			
		});
		tel_view.addEventListener("touchend",function(){
			tel_view.backgroundColor="#cccccc";
			
		});
		
		
		tel_view.addEventListener("touchcancel",function(){
			tel_view.backgroundColor="#cccccc";
			
		});
		
		tel_view.addEventListener('click',function(){
			Titanium.Platform.openURL('tel:00306944640082');
		});
		
		
		
		var email_view = Ti.UI.createView({
			top:315,
			backgroundColor:"#cccccc",
			width: '95%',
			height: '40',
			borderRadius:6
		});
		
		var email_lbl = Ti.UI.createLabel({
				text:"Info@studioone.gr",
				color:"black",
				font:{fontSize:12,fontWeight:"bold"},
				width:"auto",
				height:'auto',
				top:10,
				left:50,
		});
		
		var email_img = Ti.UI.createImageView({
			image:'images/18-envelope.png',
			top:8,
			left:10,
			width:26,
			height:21
		});
		
		email_view.addEventListener("touchstart",function(){
			email_view.backgroundColor="#d2dfc3";
			
		});
		email_view.addEventListener("touchend",function(){
			email_view.backgroundColor="#cccccc";
			
		});
		email_view.addEventListener("touchcancel",function(){
			email_view.backgroundColor="#cccccc";
			
		});
		
		email_view.add(arrow2_img);
		email_view.add(email_img);
		email_view.add(email_lbl);
		
		email_view.addEventListener('click',function(){
			sendEmail();
			
		});
		
		
		place_view.addEvent
		
		self.add(contact_lbl);
		self.add(sharing_fb_btn);
		self.add(sharing_tw_btn);
		self.add(sharing_tube_btn);
		self.add(place_view);
		self.add(tel_view);
		self.add(email_view);
		
	//})
	
return self;
};

module.exports = createTab4;