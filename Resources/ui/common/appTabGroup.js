function AppTabGroup(Window) {
	
	var self = Ti.UI.createTabGroup();
	
	var createTab1 = require('ui/common/uitab1');
	var createTab2 = require('ui/common/uitab2');
	var createTab3 = require('ui/common/uitab3');
	var createTab4 = require('ui/common/uitab4');
	
	var win1 = new createTab1(new Window(L('Αρχική'))),
	 win2 = new createTab2(new Window(L('Αρχική'))),
	 win3 = new createTab3(new Window(L('Αρχική'))),
	 win4 = new createTab4(new Window(L('Αρχική')));
	
	var tab1 = Ti.UI.createTab({
		title: L('Αρχική'),
		icon: '/images/arxiki.png',
		window: win1
	});
	win1.containingTab = tab1;
	
	
	
	var tab2 = Ti.UI.createTab({
		title: L('Νέα'),
		icon: '/images/news.png',
		window: win2
	});
	win2.containingTab = tab2;
	
	var tab3 = Ti.UI.createTab({
		title: L('studiOne'),
		icon: '/images/studione.png',
		window: win3
	});
	win3.containingTab = tab3;
	
	var tab4 = Ti.UI.createTab({
		title: L('Επικοινωνία'),
		icon: '/images/contact.png',
		window: win4
	});
	win4.containingTab = tab4;
	
	self.addTab(tab1);
	self.addTab(tab2);
	self.addTab(tab3);
	self.addTab(tab4);
	
	
	return self;
};

module.exports = AppTabGroup;