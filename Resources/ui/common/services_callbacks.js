var res_home="";
var res_info="";
var res_news=[];
var res_instr=[];	
var res_cert=[];

function home_page_clb(resp){
	var res = JSON.parse(resp);
	var value = decodeURI(res.result);
	var html='<html><body style="margin:0;padding:0;color:#D2DFC3;"><div style="-webkit-border-radius:15px;padding:15px;">'+value+'</div><body><html>';
	res_home = html;
	
};

function info_page_clb(resp){
	var res = JSON.parse(resp);
	var html = '<html><body style="margin:0;padding:0;color:#D2DFC3;"><div style="-webkit-border-radius:15px;padding:15px;">'+res.result[0].infopage+'</div></body></html>';
	res_info=html;
	
};

function refresh_news(resp,tableview)
{
	
	news_clb(resp);
	tableview.setData(res_news);
}

function news_clb(resp){
	
	var res = JSON.parse(resp);
	
	var data=[];
	for(var i=0;i<res.result.length;i++)
	{
		

		var rowcolor = (i%2 == 0) ? '#333' : '#666';
		var row = Ti.UI.createTableViewRow();
		row.backgroundColor = "transparent";
		row.selectedBackgroundColor="#333";	
		row.height = 50;
		row.className = 'datarow';
		row.clickName = 'row';
		row.news_id = res.result[i].id;
		row.news_title = res.result[i].title;
		row.news_text = res.result[i].text;
		row.news_date = res.result[i].date;
		row.news_time = res.result[i].time;
		row.news_image = res.result[i].image;
		row.news_description = res.result[i].description;
		row.news_link = res.result[i].link;
		
		
		var news_date_title = Ti.UI.createLabel({
			color:'#D2DFC3',
			font:{fontSize:11,fontWeight:'bold', fontFamily:'Arial'},
			left:5,
			top:4,
			width:250,
			clickName:'newsDateTitle',
			text:"Ημ/νία:"
		});
		
		var news_date = Ti.UI.createLabel({
			color:'#D2DFC3',
			font:{fontSize:11,fontWeight:'bold', fontFamily:'Arial'},
			left:50,
			top:4,
			width:250,
			clickName:'date',
			text:decodeURI(res.result[i].date)
		});
		
		var news_title = Ti.UI.createLabel({
			color:'#D2DFC3',
			font:{fontSize:14,fontWeight:'bold', fontFamily:'Arial'},
			left:5,
			top:22,
			width:250,
			height:20,
			ellipsize:true,
			clickName:'newsTitle',
			text:decodeURI(res.result[i].title)
		});
		
		var news_back_row = Ti.UI.createView({
			backgroundColor:rowcolor,
			opacity:0.7,
			width:"auto",
			height:"auto",
			top:0,
			left:0
		});
		
		var arrow_img = Ti.UI.createImageView({
			image:'images/listArrow.png',
			top:15,
			right:5,
			width:25,
			height:20
		});
		
		
		row.add(news_back_row);
		row.add(news_date_title);
		row.add(news_date);
		row.add(news_title);
		row.add(arrow_img);


		
		data.push(row);
	}
	res_news=data;
};



function certifications_clb(resp){
	var res = JSON.parse(resp);
	
		
	var data=[];
	for(var i=0;i<res.result.length;i++)
	{
		var rowcolor = (i%2 == 0) ? '#ddd' : '#999';
		var row = Ti.UI.createTableViewRow();
		row.backgroundColor = "transparent";
		row.selectedBackgroundColor="#999";	
		row.height = 45;
		row.className = 'datarow';
		row.clickName = 'row';
		row.vid = res.result[i].vid;
		row.mlid = res.result[i].mlid
		
		var licence_title = Ti.UI.createLabel({
			color:'#D2DFC3',
			font:{fontSize:16,fontWeight:'bold', fontFamily:'Arial'},
			left:5,
			top:5,
			height:30,
			width:250,
			clickName:'user',
			text:decodeURI(res.result[i].title)
		});
		
		var arrow_img = Ti.UI.createImageView({
			image:'images/listArrow.png',
			top:10,
			right:5,
			width:25,
			height:20
		});
		
		row.add(arrow_img);
		row.add(licence_title);


		
		data.push(row);
	}
	res_cert = data;
};


function instructors_clb(resp){
	var res = JSON.parse(resp);
					
	var data=[];
	for(var i=0;i<res.result.length;i++)
	{
		
		var rowcolor = (i%2 == 0) ? '#D2DFC3' : 'white';
		
		var row = Ti.UI.createTableViewRow();
		row.backgroundColor = "transparent";
		row.selectedBackgroundColor="#999";				
		row.height = 60;
		row.className = 'datarow';
		row.clickName = 'row';
		row.vid = res.result[i].vid;
		row.username = res.result[i].name
		
		var photo = Ti.UI.createImageView({
			image:res.result[i].image,
			top:5,
			left:10,
			width:50,
			height:50,
			clickName:'photo'
		});
		row.add(photo);
		
		var user = Ti.UI.createLabel({
			color:'#D2DFC3',
			font:{fontSize:16,fontWeight:'bold', fontFamily:'Arial'},
			left:70,
			top:15,
			height:30,
			width:200,
			clickName:'user',
			text:res.result[i].name
		});
		
		var arrow_img = Ti.UI.createImageView({
			image:'images/listArrow.png',
			top:20,
			right:5,
			width:25,
			height:20
		});
		row.add(arrow_img);	
		row.add(user);					
		data.push(row);
	}
	
	res_instr = data;		
	
};

