function createTab3(win3) {
	self = win3;
	
	// ============================Starting Licences=====================
	var toolActInd = Titanium.UI.createActivityIndicator();
	
	toolActInd.style = Titanium.UI.iPhone.ActivityIndicatorStyle.PLAIN;
	toolActInd.font = {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'};
	toolActInd.color = 'white';
	toolActInd.message = 'Loading...';
	
		
	
	
	// Create Licences Window
	var licence_win = Titanium.UI.createWindow({
		//title:res.result[0].name,									
		backgroundImage:"images/bg_app.jpg",
		barColor:'#000',
		//translucent:true
	});
	
	var licence_win_winview = Titanium.UI.createView({									
		width:'95%',
		height:'95%',
		borderRadius:10,
		backgroundColor:"black",
		opacity:"0.6",
		top:10
		
	});

	licence_win.add(licence_win_winview);
	
	// END Create Licences Window
	
	function getSubLicences(params)
	{
			
		if(networkStatus)
		{
			var serv1 = "http://studioone.danisoft.gr/services_iphone/json/iphonev2";
						
			var paramsObj = {"id":2,"jsonrpc":"2.0","method":"get_subcategory_certifications","client":"iphone","params":[params.mlid]};
			
			var parmsStr = JSON.stringify(paramsObj);
			var tokenRequest = Ti.Network.createHTTPClient({timeout:60000});
			//tokenRequest.ondatastream = function(e){Ti.API.log(e.progress)}
			tokenRequest.open('POST', serv1);
			tokenRequest.onload = function() 
				{ 
					toolActInd.hide();
					win3.setToolbar(null,{animated:true});
					licence_win.setTitle(params.res.title);
					licence_win.backButtonTitle = 'Πίσω';	
					var res = JSON.parse(decodeURI(this.responseText));
					
					var licence_webView = Ti.UI.createWebView({
					top:12,
					height:"92%",
					width:"90%",
					borderRadius:15,
					font:{fontSize:12,fontWeight:'bold', fontFamily:'Arial'},
					backgroundColor:"transparent",
					showScrollbars:false
			
					});
					licence_win.add(licence_webView);
					
					var subCat = '-';
					if(res.result!=null){
						var sub_str ='';
						for(var i=0;i<res.result.length;i++)
						{
							var comma = (i==0)?'':', ';
							sub_str += comma + res.result[i].title;
						}
						subCat = sub_str;
						
					}
					
					
					var html='<!DOCTYPE html>'+
					'<html lang="en"> <head>'+		
					'<meta http-equiv="Content-Type" content="text/html; charset=utf-8">'+
					'<style>body { -webkit-text-size-adjust:none;} </style>'+
					'</head>'+					
					'<body style="font-family:Arial;font-weight:bold;font-size:12px;margin:0;padding:0;color:#D2DFC3;">'+
					'<div style="-webkit-border-radius:15px;padding:15px;">'+
					'<div style="font-size:16px;">'+params.res.title+'</div>'+
					'<div style="padding-top:15px;width:100%; text-align:center">'+
					'<img align="center" src="'+params.res.image+'"/></div>'+
					'<div style="padding-top:15px;">Υποκατηγορίες : '+subCat+'</div>'+
					'<div style="padding-top:15px;">'+params.res.description+'</div>'+
					'</div>'+
					'</body></html>';
		
					
					licence_webView.setHtml(html);
					licence_webView.addEventListener('load',licence_webView_listener);
								
						
				};
			tokenRequest.onerror = function(e){
					Ti.API.info("CallServices error = " + e.error);
					can_press = true;
					win3.setToolbar(null,{animated:true});	
					noConection_alert.show();
					toolActInd.hide();
				}
			
			tokenRequest.setRequestHeader("contentType", "application/json-rpc"); 
			tokenRequest.setRequestHeader("dataType", "json");
			tokenRequest.send(parmsStr); 
		}else{
			noConection_alert.show();
		}
		
	}
	
	var licence_webView_listener = function(){
		toolActInd.hide();
		win3.containingTab.open(licence_win,{animated:true});
		//This is needed because if you press the table twice before content is loaded and the view is visible 
		//you will get an error
		can_press = true;
					
	}
		
	licence_win.addEventListener('close',function(){
		licence_win.children[1].removeEventListener('load',licence_webView_listener);
		licence_win.remove(licence_win.children[1]);
	});
	
	
	
	var mainView = Ti.UI.createView({
		top: 0,
		left: 0,
		width: '100%',
		height: '100%',
		layout: 'vertical'
		});
		
	self.add(mainView);
		
	var tabbedBar = Titanium.UI.iOS.createTabbedBar({
			labels: ['Καθηγητές','Διπλώματα'],
			color:"red",
			backgroundColor: '#999',
			style: Ti.UI.iPhone.SystemButtonStyle.BAR,
			height: 30,
			width: '95%',
			top: 5,
			index:0
		});
	mainView.add(tabbedBar);
		
	var choiceView = Ti.UI.createView({
			top: 0,
			left: 0,
			width: '100%',
			height: 'auto'
	});
	mainView.add(choiceView);
		
	var licenceView = Ti.UI.createView({
			width: '95%',
			height: 'auto',
			left: '2.5%',
			top:5
			
	});
	
	var licence_tableview = Titanium.UI.createTableView({
					backgroundColor:'transparent',
					separatorStyle:Ti.UI.iPhone.TableViewSeparatorStyle.NONE,
					borderRadius:5,
				});
	
				
	licenceView.add(licence_tableview);
				
	licence_tableview.addEventListener('click', function(e)
	{
		
		if(networkStatus)
		{
			if(can_press){
				can_press = false
				win3.setToolbar([toolActInd],{animated:true});
				toolActInd.show();
				
				var serv1 = "http://studioone.danisoft.gr/services_iphone/json/iphonev2";
			
				var paramsObj = {"id":2,"jsonrpc":"2.0","method":"get_certification","client":"iphone","params":[e.rowData.vid]};
				var parmsStr = JSON.stringify(paramsObj);
				var tokenRequest = Ti.Network.createHTTPClient({timeout:60000}); 
				tokenRequest.open('POST', serv1);
				tokenRequest.onload = function() 
					{ 
						var res = JSON.parse(decodeURI(this.responseText));
						var myparam = {mlid:e.rowData.mlid,res:res.result[0]};
						getSubLicences(myparam);	
											
					};
				tokenRequest.onerror = function(e){
					Ti.API.info("CallServices error = " + e.error);
					can_press = true;
					win3.setToolbar(null,{animated:true});	
					noConection_alert.show();
					toolActInd.hide();
				}
				
				tokenRequest.setRequestHeader("contentType", "application/json-rpc"); 
				tokenRequest.setRequestHeader("dataType", "json");
				tokenRequest.send(parmsStr); 
				}
		}else{
			noConection_alert.show();
		}
		
		
	});
	
	
	licenceView.hide();
		//getLicences(licenceView);
	choiceView.add(licenceView);
		
	var profView = Ti.UI.createView({
			width: '95%',
			height: 'auto',
			left: '2.5%',
			top:5
			
		});
		
	choiceView.add(profView);
	
	// Profesors ==========
	var prof_win = Titanium.UI.createWindow({
		//title:res.result[0].name,									
		backgroundImage:"images/bg_app.jpg",
		barColor:'#000',
		//translucent:true
	});
	
	var prof_win_winview = Titanium.UI.createView({									
		width:'95%',
		height:'95%',
		borderRadius:10,
		backgroundColor:"black",
		opacity:"0.6",
		top:10
		
	});

	
	prof_win.add(prof_win_winview);

	var can_press = true;
	
	var prof_tableview = Titanium.UI.createTableView({
					backgroundColor:'transparent',
			
					separatorStyle:Ti.UI.iPhone.TableViewSeparatorStyle.NONE,
					borderRadius:5,
	});
				
	prof_tableview.addEventListener('click', function(e)
	{
		if(networkStatus)
		{
			if(can_press){
			can_press = false
			
			win3.setToolbar([toolActInd],{animated:true});
			toolActInd.show();
			var serv1 = "http://studioone.danisoft.gr/services_iphone/json/iphonev2";
		
			var paramsObj = {"id":2,"jsonrpc":"2.0","method":"instructor_biografy","client":"iphone","params":[e.rowData.vid]};
			//alert(e.rowData.vid)
			var parmsStr = JSON.stringify(paramsObj)
		
			var tokenRequest = Ti.Network.createHTTPClient({timeout:60000}); 
			tokenRequest.open('POST', serv1);
			tokenRequest.onload = function() 
				{ 
					
					win3.setToolbar(null,{animated:true});	
					
					var res = JSON.parse(decodeURI(this.responseText));
					prof_win.setTitle(res.result[0].name);
					prof_win.backButtonTitle = 'Πίσω';	
					
					var prof_webView = Ti.UI.createWebView({
					top:12,
					height:"92%",
					width:"90%",
					borderRadius:15,
					font:{fontSize:12,fontWeight:'bold', fontFamily:'Arial'},
					backgroundColor:"transparent",
					showScrollbars:false
			
					});
					
					var email_info="-";
					if(res.result[0].email!=null){
						email_info = '<span id="myemail" style="width:100%;word-wrap: break-word;text-overflow: ellipsis;text-decoration:underline">'+
						res.result[0].email+'<span>';
						
					}
						
						
					var website_info="-"
					if(res.result[0].website !=null){
						website_info = '<span id="myurl" style="width:100%;word-wrap: break-word;text-overflow: ellipsis;text-decoration:underline">'+
						res.result[0].website+'<span>';
						}
					
					var cert_res ="-";
					if(res.result[0].certifications!=null || res.result[0].certifications.length>0)
					{
						cert_res ="";	
							if((res.result[0].certifications.length==1) && (res.result[0].certifications[0]==null)){
								cert_res ="-";
							}else{					
								for(var i=0;i<res.result[0].certifications.length;i++)
								{
									if(res.result[0].certifications[i]!=null)
									{
										var comma = (i==0)?'':', ';
										cert_res += comma+res.result[0].certifications[i];
									}						
								}
							}
						
					}
					
					var myscript = '<script>'+
					'function test(){'+
					'var mydiv = document.getElementById("myurl"); '+
					'var myemail = document.getElementById("myemail"); '+
					'if(mydiv){'+
					'mydiv.onclick=function(){'+
					'Ti.App.fireEvent(\'openURL\',{ url: \''+res.result[0].website+'\'});'+
					'}}'+
					'if(myemail){'+
					'myemail.onclick=function(){'+
					'Ti.App.fireEvent(\'openEmail\',{ email: \''+res.result[0].email+'\'});'+
					'}}'+
					'} </script>';
					
					var html='<!DOCTYPE html>'+
					'<html lang="en"> <head>'+myscript+					
					'<meta http-equiv="Content-Type" content="text/html; charset=utf-8">'+
					'<style>body { -webkit-text-size-adjust:none;} </style>'+
					'</head>'+
					'<body onload="test();" style="font-family:Arial;font-weight:bold;font-size:12px;margin:0;padding:0;color:#D2DFC3;">'+
					'<div style="-webkit-border-radius:15px;padding:15px;">'+
					'<div style="width:100%">'+
					'<div style="width:30%;float:left;"><img style="width:100%" src="'+res.result[0].image+'"/></div>'+
					'<div style="width:70%;float:left; font-size:14px; text-align:center; padding-top:30px;">'+res.result[0].name+'</div>'+
					'<div style="clear:both"></div></div>'+
					'<div style="padding-top:15px;">Email: '+email_info+'</div>'+
					'<div style="padding-top:15px;">Website: '+website_info+'</div>'+
					'<div style="padding-top:15px;">Διπλώματα: '+cert_res+'</div>'+
					'<div style="padding-top:15px;">'+res.result[0].biography+'</div>'
					'</div>'+
					'</body></html>';
		
					
					prof_win.add(prof_webView)
					prof_webView.setHtml(html);
					
					
					prof_webView.addEventListener('load',prof_webView_listener);
						
				};
				
				
				tokenRequest.onerror = function(e){
					Ti.API.info("CallServices error = " + e.error);
					can_press = true;
					win3.setToolbar(null,{animated:true});	
					noConection_alert.show();
					toolActInd.hide();
					
				}
			tokenRequest.setRequestHeader("contentType", "application/json-rpc"); 
			tokenRequest.setRequestHeader("dataType", "json");
			tokenRequest.send(parmsStr); 
			}
			
		}else{
			noConection_alert.show();
		}
		
		
	});
	
	var prof_webView_listener = function(){
		toolActInd.hide();
		win3.containingTab.open(prof_win,{animated:true});
		//This is needed because if you press the table twice before content is loaded and the view is visible 
		//you will get an error
		can_press = true;
					
	}
		
	prof_win.addEventListener('close',function(){
		prof_win.children[1].removeEventListener('load',prof_webView_listener);
		prof_win.remove(prof_win.children[1]);
	});
		
	profView.add(prof_tableview);
	
	win3.addEventListener("open",function(){
		prof_tableview.setData(res_instr);
		licence_tableview.setData(res_cert);
		if(Titanium.UI.iPhone.appBadge > 0)
		{
			Titanium.UI.iPhone.appBadge = null;
		}
	});
		
	tabbedBar.addEventListener('click',function(e) {
			
			switch(e.index) {
				case 0: // prof
					licenceView.hide();
					profView.show();
					break;
				case 1: // licence
					profView.hide();
					licenceView.show();
					break;	
			}
		});
			
				
	
	
	

return self;
};

module.exports = createTab3;