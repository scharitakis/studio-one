function createTab1(win1) {
	self = win1;
	
	// School Btn----Window 1---------------------
	
	var schlbtn = Titanium.UI.createButton({title:'Σχολή'});
	self.leftNavButton = schlbtn;
	var w_school = Titanium.UI.createWindow({
			backgroundImage:"images/bg_app.jpg",
			title:'Προφίλ',
			barColor:'black'
		});
	var w_school_btn_close = Titanium.UI.createButton({
			title:'Πίσω',
			style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
		});
	w_school.setLeftNavButton(w_school_btn_close);
	w_school_btn_close.addEventListener('click',function()
		{
			w_school.remove(w_school.children[1])
			w_school.close();
		});
	
	var school_winview = Titanium.UI.createView({									
									width:'95%',
									height:'95%',
									borderRadius:10,
									backgroundColor:"black",
									opacity:"0.6",
									top:10,
									
								});
	w_school.add(school_winview);
	
	
	
	schlbtn.addEventListener('click', function()
	{
		var school_info = Ti.UI.createWebView({
			color:'#D2DFC3',
			backgroundColor:"transparent",
			font:{fontSize:12,fontWeight:'bold', fontFamily:'Arial'},
			top:12,
			height:"95%",
			width:"90%"
			
		});
		school_info.setHtml(res_info);
		w_school.add(school_info);
		w_school.open({modal:true});
	});
	
	
	
	var win1_winview_background = Titanium.UI.createView({									
		width:'95%',
		height:'94%',
		borderRadius:10,
		backgroundColor:"black",
		opacity:"0.6",
		top:10
		
	});
	
	
	var win1_webview = Titanium.UI.createWebView(
				{
					top:12,
					height:"92%",
					width:"90%",
					borderRadius:15,
					font:{fontSize:12,fontWeight:'bold', fontFamily:'Arial'},
					backgroundColor:"transparent",
					showScrollbars:false
					}
				);
	self.add(win1_winview_background);	
	self.add(win1_webview);	
			
	
	self.addEventListener('open', function()
	{
		win1_webview.setHtml(res_home);
	});
	
	//Tab1 End ------------------------------
	
	return self;
};

module.exports = createTab1;