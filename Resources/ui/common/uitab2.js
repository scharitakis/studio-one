//Titanium.include('ui/common/services.js');
//Titanium.include('ui/common/services_callbacks.js');

function createTab2(win2) {
	self = win2

	var newsView = Ti.UI.createView({
		top:10,
		width: '95%',
		height: 'auto'
	});
	win2.add(newsView);
	
	
	
	var tableview = Titanium.UI.createTableView({
					backgroundColor:'transparent',
					separatorStyle:Ti.UI.iPhone.TableViewSeparatorStyle.NONE,
					borderRadius:10
	});
				
				
	newsView.add(tableview);
				
	var refresh_btn = Titanium.UI.createButton({
		systemButton:Titanium.UI.iPhone.SystemButton.REFRESH
	});
	
	win2.rightNavButton = refresh_btn;
	
	refresh_btn.addEventListener("click",function()
	{
		var serviceCalls =[
		{
			name:"news",
			host:"http://studioone.danisoft.gr/notification/json/notifications",
			params:{"id":2,"jsonrpc":"2.0","method":"news"},
			callback:refresh_news,
			clb_params:tableview
		}		
		];
		startServices(serviceCalls,0);	
		//tableview.setData(res_news);
	});
	
	win2.addEventListener("open",function(){
		Titanium.UI.iPhone.appBadge = null;
		tableview.setData(res_news);
	});
		
	var news_win = Titanium.UI.createWindow({
		//title:res.result[0].name,									
		backgroundImage:"images/bg_app.jpg",
		barColor:'#000',
		//translucent:true
	});
	
	var news_win_winview = Titanium.UI.createView({									
		width:'95%',
		height:'95%',
		borderRadius:10,
		backgroundColor:"black",
		opacity:"0.6",
		top:10
		
	});
	
	
	news_win.add(news_win_winview);
	
	tableview.addEventListener('click', function(e)
	{
		news_win.setTitle(e.rowData.news_title);
		news_win.backButtonTitle = 'Πίσω';	
		var news_webView = Ti.UI.createWebView({
					top:12,
					height:"92%",
					width:"90%",
					borderRadius:15,
					font:{fontSize:12,fontWeight:'bold', fontFamily:'Arial'},
					backgroundColor:"transparent",
					showScrollbars:false
			
		});
		
		var img ='';
		
		if(e.rowData.news_image!=""){img='<div style="text-align:center;width:100%;"><img style="width:200px;"src="'+e.rowData.news_image+'"/></div>'}
		var link = (e.rowData.news_link=="")?'Link: -':'Link: <div id="myurl" style="width:100%;word-wrap: break-word;text-overflow: ellipsis;text-decoration:underline">'+e.rowData.news_link+'</div>';
		
		var html='<!DOCTYPE html>'+
		'<html lang="en"> <head><script>function test(){var mydiv = document.getElementById("myurl"); if(mydiv){mydiv.onclick=function(){Ti.App.fireEvent(\'openURL\',{ url: \''+e.rowData.news_link+'\'})}}  } </script> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> </head>'+
		'<body onload="test();" style="font-family:Arial;font-weight:bold;font-size:12px;margin:0;padding:0;color:#D2DFC3;"><div style="-webkit-border-radius:15px;padding:15px;">Ημ/νία: '+e.rowData.news_date+
		'<div style="font-size:14px;padding-top:15px;padding-bottom:10px;">'+e.rowData.news_title+'</div><div>'+img+'</div>'+
		'<div style="padding-top:10px;">'+link +'</div><div style="padding-top:10px;">'+e.rowData.news_description+'</div></br></div></body></html>';
		
		news_win.add(news_webView)
		news_webView.setHtml(html);
		
		
		win2.containingTab.open(news_win,{animated:true});
		
	});	
		
	news_win.addEventListener('close',function(){
		news_win.remove(news_win.children[1]);
	});
		

return self;
};

module.exports = createTab2;