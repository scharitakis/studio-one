var servicesStatus = false;

function callService(host,params,callback,call_params)
{
		var parmsStr = JSON.stringify(params)
		var tokenRequest = Ti.Network.createHTTPClient({timeout:46000});
		tokenRequest.open('POST', host);
		tokenRequest.onload = function(e){				
			var res = this.responseText
			if(call_params==null){
				callback(res);
			}
			else{
				callService(res,call_params);
			}
		};
		tokenRequest.onerror = function(e){
				Ti.API.info("CallServices error = " + e.error);
				connectionError_alert.show();
		};
		//tokenRequest.setTimeout(36000);
		tokenRequest.setRequestHeader("contentType", "application/json-rpc"); 
		tokenRequest.setRequestHeader("dataType", "json");
		tokenRequest.send(parmsStr); 
	
}


function startServices(services,serviceNum){
		ind.message = 'Loading ..';//Service ' + (serviceNum+1) + ' of '+services.length;
		
		callService(services[serviceNum].host,services[serviceNum].params,function(res){
			services[serviceNum].callback(res,services[serviceNum].clb_params);
			
			if(serviceNum<services.length-1){
				startServices(services,serviceNum+1);
				ind.value = serviceNum+1;
			}
			else{
				servicesStatus = true;
				ind.value = serviceNum+1;
				tabGroup.open();
				win.close();
			}
			
		});	
}