Titanium.include('ui/common/pushNotification.js');
Titanium.include('ui/common/services.js');
Titanium.include('ui/common/services_callbacks.js');
var AppTabGroup = require('ui/common/appTabGroup');
var Window = require('ui/handheld/ApplicationWindow');

/*Titanium.App.iOS.registerBackgroundService({
   url:'bg.js'
});*/

Ti.App.addEventListener('openURL', function(e){
    Titanium.Platform.openURL(e.url);
});

Ti.App.addEventListener('openEmail', function(e){
	
	var emailDialog = Ti.UI.createEmailDialog()
	    	emailDialog.subject = "StudioOne";
	    	emailDialog.toRecipients = [e.email];
	    	emailDialog.messageBody = '';
	    	emailDialog.open();
   
});


Ti.App.addEventListener('opentel', function(e){
	Titanium.Platform.openURL('tel:'+e.tel);
   
});

var serviceCalls =[
		{
			name:"home_page",
			host:"http://studioone.danisoft.gr/services_iphone/json/iphonev2",
			params:{"id":2,"jsonrpc":"2.0","method":"home_page","client":"iphone"},
			callback:home_page_clb
		},
		{
			name:"info_page",
			host:"http://studioone.danisoft.gr/services_iphone/json/iphonev2",
			params:{"id":2,"jsonrpc":"2.0","method":"get_info_page","client":"iphone"},
			callback:info_page_clb
		},{
			name:"news",
			//host:"http://studioone.danisoft.gr/notification/json/notifications",
			host:"http://studioone.danisoft.gr/notification/json/notifications",
			params:{"id":2,"jsonrpc":"2.0","method":"news"},
			callback:news_clb
		},
		{
			name:"certifications",
			host:"http://studioone.danisoft.gr/services_iphone/json/iphonev2",
			params:{"id":2,"jsonrpc":"2.0","method":"get_category_certifications","client":"iphone"},
			callback:certifications_clb
		},
		{
			name:"instructors",
			host:"http://studioone.danisoft.gr/services_iphone/json/iphonev2",
			params: {"id":2,"jsonrpc":"2.0","method":"get_instructors","client":"iphone"},
			callback:instructors_clb
		}
		
		];


if (Ti.version < 1.8 ) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}



var iPhone5 = ""
if(Ti.Platform.displayCaps.platformHeight==568)
{
	var iPhone5 = "-568h@2x"
}
var win = Titanium.UI.createWindow({width:"100%",height:"100%",backgroundImage:"images/bgLoaderScreen"+iPhone5+".png"});
//var loadview = Titanium.UI.createView({width:"100%",height:"100%",backgroundImage:"iphone/test1.png"})
//win.add(loadview);
var ind=Titanium.UI.createProgressBar({
	width:150,
	min:0,
	max:serviceCalls.length,
	value:0,
	height:70,
	color:'#D2DFC3',
	message:'Loading..',
	font:{fontSize:12, fontWeight:'bold'},
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	top:300
});
win.add(ind);

win.open();

var tabGroup = Ti.UI.createTabGroup();

var noConection_alert = Titanium.UI.createAlertDialog({
		message:'Η εφαρμογή απαιτεί σύνδεση με το Internet.Παρακαλώ συνδεθείτε.'
});

var connectionError_alert = Titanium.UI.createAlertDialog({
		message:'Υπάρχει πρόβλημα με την σύνδεση. Παρακαλώ προσπαθήστε αργότερα'
});



var tabGroup = new AppTabGroup(Window)

var networkStatus = false
if(Titanium.Network.online==1)
{
	networkStatus = true;
	ind.show();
	addPushNotification(tabGroup);
	startServices(serviceCalls,0);
}
else{

	noConection_alert.show();
}

Titanium.Network.addEventListener('change', function(e)
{
	if(Titanium.Network.online==1)
	{
		networkStatus = true;
		if(!servicesStatus)
		{
			ind.show();
			addPushNotification();
			startServices(serviceCalls,0);
		}
	}
	else{
		networkStatus = false;
		noConection_alert.show();
	}
});



